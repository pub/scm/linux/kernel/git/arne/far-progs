/*
 * Copyright (C) 2012, 2013 STRATO AG.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/*
 * Handling of extended attributes aka xattr or extattr is located in this
 * file. It is different for Linux, SunOS and all the other operating systems.
 * Refer to <http://en.wikipedia.org/wiki/Extended_file_attributes>.
 * This current implementation supports Linux and SunOS.
 * These functions are used internally in the far-rcv library.
 */

#if !defined (__FAR_RCV_XATTR_H)
#define __FAR_RCV_XATTR_H

int far_rcv_lsetxattr(const char *path, const char *name, const void *value,
		      size_t size);
int far_rcv_lremovexattr(const char *path, const char *name);

#endif /* !defined (__FAR_RCV_XATTR_H) */
