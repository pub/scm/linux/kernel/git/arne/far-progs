/*
 * Copyright (C) 2012 Alexander Block.
 * Copyright (C) 2012, 2013 STRATO AG.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/*
 * This is the main part of the far-rcv library. All exported types and
 * functions are located in this file.
 */

#ifndef FAR_RCV_H_
#define FAR_RCV_H_

#include <stdint.h>
#include <time.h>

#define FAR_UUID_SIZE 16


struct far_rcv_ctx;

struct far_rcv_ops {
	int (*finish_subvol)(struct far_rcv_ctx *frctx);
	int (*subvol)(struct far_rcv_ctx *frctx, const char *path,
		      const unsigned char *uuid, uint64_t ctransid);
	int (*snapshot)(struct far_rcv_ctx *frctx, const char *path,
			const unsigned char *uuid, uint64_t ctransid,
			const unsigned char *parent_uuid,
			uint64_t parent_ctransid);
	int (*mkfile)(struct far_rcv_ctx *frctx, const char *path);
	int (*mkdir)(struct far_rcv_ctx *frctx, const char *path);
	int (*mknod)(struct far_rcv_ctx *frctx, const char *path, uint64_t mode,
		     uint64_t dev);
	int (*mkfifo)(struct far_rcv_ctx *frctx, const char *path);
	int (*mksock)(struct far_rcv_ctx *frctx, const char *path);
	int (*symlink)(struct far_rcv_ctx *frctx, const char *path,
		       const char *lnk);
	int (*rename)(struct far_rcv_ctx *frctx, const char *from,
		      const char *to);
	int (*link)(struct far_rcv_ctx *frctx, const char *path,
		    const char *lnk);
	int (*unlink)(struct far_rcv_ctx *frctx, const char *path);
	int (*rmdir)(struct far_rcv_ctx *frctx, const char *path);
	int (*open_inode_for_write)(struct far_rcv_ctx *frctx,
				    const char *path);
	int (*close_inode_for_write)(struct far_rcv_ctx *frctx);
	int (*write)(struct far_rcv_ctx *frctx, const char *path,
		     const void *data, uint64_t offset, uint64_t len);
	int (*clone)(struct far_rcv_ctx *frctx, const char *path,
		     uint64_t offset, uint64_t len,
		     const unsigned char *clone_uuid, uint64_t clone_ctransid,
		     const char *clone_path, uint64_t clone_offset);
	int (*set_xattr)(struct far_rcv_ctx *frctx, const char *path,
			 const char *name, const void *data, int len);
	int (*remove_xattr)(struct far_rcv_ctx *frctx, const char *path,
			    const char *name);
	int (*truncate)(struct far_rcv_ctx *frctx, const char *path,
			uint64_t size);
	int (*chmod)(struct far_rcv_ctx *frctx, const char *path,
		     uint64_t mode);
	int (*chown)(struct far_rcv_ctx *frctx, const char *path, uint64_t uid,
		     uint64_t gid);
	int (*utimes)(struct far_rcv_ctx *frctx, const char *path,
		      struct timespec *at, struct timespec *mt,
		      struct timespec *ct);
};

struct far_rcv_ctx {
	struct far_rcv_ops ops;

	int verbose;
	int support_xattrs;
	int honor_end_cmd;
	int free_current_base_path;
	const char *current_base_path;
	int write_fd;
	const char *write_path;
	const char *root_path;
};

int far_rcv_init(struct far_rcv_ctx *frctx);
void far_rcv_deinit(struct far_rcv_ctx *frctx);
int far_rcv_mainloop(struct far_rcv_ctx *frctx, int stream_fd,
		     const char *root_path);
char *far_rcv_path_cat(const char *p1, const char *p2);

#endif /* !defined (FAR_RCV_H_) */
