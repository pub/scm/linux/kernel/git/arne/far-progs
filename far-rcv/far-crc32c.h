/*
 * This file is derived from work of Ross N. Williams, which is described in
 * the document "A Painless Guide to CRC Error Detection Algorithms".
 * The document can be found here:
 * <http://www.ross.net/crc/download/crc_v3.txt>
 *
 * It contains the following copyright and status information:
 * ----- quote start -----
 * Version : 3.
 * Date    : 19 August 1993.
 * Author  : Ross N. Williams.
 * Net     : ross@guest.adelaide.edu.au.
 * FTP     : ftp.adelaide.edu.au/pub/rocksoft/crc_v3.txt
 * Company : Rocksoft^tm Pty Ltd.
 * Snail   : 16 Lerwick Avenue, Hazelwood Park 5066, Australia.
 * Fax     : +61 8 373-4911 (c/- Internode Systems Pty Ltd).
 * Phone   : +61 8 379-9217 (10am to 10pm Adelaide Australia time).
 * Note    : "Rocksoft" is a trademark of Rocksoft Pty Ltd, Australia.
 * Status  : Copyright (C) Ross Williams, 1993. However, permission is
 *           granted to make and distribute verbatim copies of this
 *           document provided that this information block and copyright
 *           notice is included. Also, the C code modules included
 *           in this document are fully public domain.
 * Thanks  : Thanks to Jean-loup Gailly (jloup@chorus.fr) and Mark Adler
 *           (me@quest.jpl.nasa.gov) who both proof read this document
 *           and picked out lots of nits as well as some big fat bugs.
 * ----- quote end -----
 *
 * The function crc32c_calc() is a slightly modified copy of the function
 * crc_reflected() from the mentioned document.
 * The modifications and the modified code are fully public domain.
 */

/*
 * This file contains a function to calculate the CRC-32C.
 * These functions are used internally in the far-rcv library.
 */

#if !defined (__FAR_RCV_CRC32C_H)
#define __FAR_RCV_CRC32C_H

#include <stdint.h>

uint32_t far_crc32c_calc(uint32_t crc, const unsigned char *blk_adr,
			 size_t blk_len);

#endif /* !defined (__FAR_RCV_CRC32C_H) */
