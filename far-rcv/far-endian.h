/*
 * Copyright (C) 2012, 2013 STRATO AG.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/*
 * Some portable functions to convert little endian numbers to the
 * host byte order.
 * It was not a goal to make these functions fast.
 * These functions are used internally in the far-rcv library.
 */

#if !defined (__FAR_RCV_ENDIAN_H)
#define __FAR_RCV_ENDIAN_H

#include <stdint.h>


#if defined (__LINUX__)
#include <endian.h>

#define far_rcv_le16toh le16toh
#define far_rcv_le32toh le32toh
#define far_rcv_le64toh le64toh

#else /* !defined (__LINUX__) */

uint16_t far_rcv_le16toh(uint16_t val);
uint32_t far_rcv_le32toh(uint32_t val);
uint64_t far_rcv_le64toh(uint64_t val);
#endif /* !defined (__LINUX__) */

#endif /* !defined (__FAR_RCV_ENDIAN_H) */
