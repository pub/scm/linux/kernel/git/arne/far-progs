/*
 * Copyright (C) 2012, 2013 STRATO AG.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 */

/*
 * Some portable functions to convert little endian numbers to the
 * host byte order.
 * It was not a goal to make these functions fast.
 * These functions are used internally in the far-rcv library.
 */

#include <sys/types.h>
#include <netinet/in.h>
#include <inttypes.h>
#include "far-endian.h"


#if !defined (__LINUX__)
static uint16_t far_rcv_le16tobe(uint16_t val);
static uint32_t far_rcv_le32tobe(uint32_t val);
static uint64_t far_rcv_le64tobe(uint64_t val);


uint16_t far_rcv_le16toh(uint16_t val)
{
	return ntohs(far_rcv_le16tobe(val));
}

uint32_t far_rcv_le32toh(uint32_t val)
{
	return ntohl(far_rcv_le32tobe(val));
}

uint64_t far_rcv_le64toh(uint64_t val)
{
#if defined (HAVE_NTOHLL)
	return ntohll(far_rcv_le64tobe(val));
#else /* !defined (HAVE_NTOHLL) */
	if (ntohs(0xbabe) != 0xbabe)
		return val;
	else
		return far_rcv_le64tobe(val);
#endif /* !defined (HAVE_NTOHLL) */
}

static uint16_t far_rcv_le16tobe(uint16_t val)
{
	return ((uint16_t)(val << 8)) | (val >> 8);
}

static uint32_t far_rcv_le32tobe(uint32_t val)
{
	uint16_t lower = (uint16_t)val;
	uint16_t upper = (uint16_t)(val >> 16);

	return (((uint32_t)far_rcv_le16tobe(lower)) << 16) |
	       (uint32_t)far_rcv_le16tobe(upper);
}

static uint64_t far_rcv_le64tobe(uint64_t val)
{
	uint32_t lower = (uint32_t)val;
	uint32_t upper = (uint32_t)(val >> 32);

	return (((uint64_t)far_rcv_le32tobe(lower)) << 32) |
	       (uint64_t)far_rcv_le32tobe(upper);
}
#endif /* !defined (__LINUX__) */
