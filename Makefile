OS = $(shell uname -s)
RELEASE = $(shell uname -r)

ifeq "$(OS)" "Linux"
CFLAGS = -D__LINUX__
CFLAGS += -DHAVE_NTOHLL
CFLAGS += -DHAVE_UTIMENSAT -I$(LIBBTRFS_INCLUDE_PREFIX)
OS_SPECIFIC_TARGET = btrfs-receive
else
ifeq "$(OS)" "SunOS"
CFLAGS = -D__SOLARIS__
ifeq "$(RELEASE)" "5.11"
CFLAGS += -DHAVE_NTOHLL
CFLAGS += -DHAVE_UTIMENSAT
endif
OS_SPECIFIC_TARGET = zfs-receive
endif
endif
CFLAGS += -Wall -D_FILE_OFFSET_BITS=64 -D_LARGE_FILES=1

LIBBTRFS_PREFIX = /usr/local
LIBBTRFS_INCLUDE_PREFIX = $(LIBBTRFS_PREFIX)/include
LIBBTRFS_LIB_PREFIX = $(LIBBTRFS_PREFIX)/lib

CC = gcc
CFLAGS += -O2 -g -Wall -Werror
DEPFLAGS = -Wp,-MMD,$(@D)/.$(@F).d,-MT,$@
LDFLAGS = $(COMMON_LIBS)
COMMON_LIBS =
FSSUM_LIBS = -lssl -lcrypto
FAR_RECEIVE_LIBS = far-rcv/far-rcv.a
BTRFS_RECEIVE_LIBS = -luuid -lpthread -L$(LIBBTRFS_LIB_PREFIX) -lbtrfs
ZFS_RECEIVE_LIBS = -lzfs

all: fardump actions commonfs-receive $(OS_SPECIFIC_TARGET) fssum

fardump: fardump.o Makefile
	$(CC) $(LDFLAGS) -o $@ fardump.o

fssum: fssum.o Makefile
	$(CC) $(LDFLAGS) -o $@ fssum.o $(FSSUM_LIBS)

commonfs-receive: commonfs-receive.o far-rcv/far-rcv.a Makefile
	$(CC) $(LDFLAGS) -o $@ commonfs-receive.o $(FAR_RECEIVE_LIBS)

btrfs-receive: btrfs-receive.o far-rcv/far-rcv.a Makefile
	$(CC) $(LDFLAGS) -o $@ btrfs-receive.o $(FAR_RECEIVE_LIBS) \
	      $(BTRFS_RECEIVE_LIBS)

zfs-receive: zfs-receive.o far-rcv/far-rcv.a Makefile
	$(CC) $(LDFLAGS) -o $@ zfs-receive.o $(FAR_RECEIVE_LIBS) \
	      $(ZFS_RECEIVE_LIBS)

actions: meta meta/*.mac
	perl expand.pl -o actions -q meta/*.mac

.PHONY: far-rcv/far-rcv.a
far-rcv/far-rcv.a:
	(cd far-rcv && $(MAKE) $(MAKEFLAGS) `basename $@`)

.c.o:
	$(CC) $(DEPFLAGS) $(CFLAGS) -c $<

clean:
	-rm -rf actions fardump fssum zfs-receive btrfs-receive \
		commonfs-receive *.o .*.d
	(cd far-rcv && $(MAKE) $(MAKEFLAGS) clean)

-include .*.d

fardump.o: fardump.c
fssum.o: fssum.c
zfs-receive.o: zfs-receive.c
btrfs-receive.o: btrfs-receive.c
common-receive.o: common-receive.c
